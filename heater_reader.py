#!/usr/bin/python

import serial, re
import paho.mqtt.publish as publish

mqttHost = "ohab"

port = serial.Serial("/dev/serial0", baudrate=19200)
mul = {0: 0.1, 1: 1, 2: 0.1, 3: 0.1, 4: 0.1, 5: 0.1}
type = {0: "TEMP", 1: "BOOL", 2: "NUM", 3: "PERCENT", 4: "AMP", 5: "KWH", 6: "H", 7: "M", 8: "DM", 9: "KW", 'A': "PULSE"}

port.flushOutput()
port.write('XR\r'.encode('utf-8'))
port.flush()

while True:
    line = port.readline()
    try:
        metric = re.sub(r'.*XR', "", line)
        regtype = int(metric[0])
        regnum =  metric[1:4]
        regval = metric[4:8]
	      if(int(regval, 16) >= 32768):
            value = (int(regval, 16) - 65536) * mul.get(regtype, 1)
        else:
            value = int(regval, 16) * mul.get(regtype, 1)

        if(regtype == 1):
            if(value == 1):
                value = "ON"
            else:
                value = "OFF"

        print type.get(regtype) + regnum, regval, value

        try:
            publish.single("/pannan/" + type.get(regtype) + regnum, value, hostname = mqttHost)
        except:
            print "could not publish to MQTT"
    except: pass