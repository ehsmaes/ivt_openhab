Use this script to log data from IVT heat pumps. The physical interface is a addon shield for raspberry pi manufactured by husdata.se. Although this script is for IVT, it should be easily adapted for other models.

The decoding of messages is derived from the developer protocol specification available on husdata.se.